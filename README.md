#Games Ranking

Um grupo de amigos costuma praticar diversas modalidades de jogos. Em suas conversas o assunto sempre é: quem é melhor, quem ganhava mais partidas. Foi aí que um deles teve a idéia de preparar um sistema que gravasse um Ranking dos jogos. Esse ranking mostraria o jogador, as vitórias e as partidas que esse jogador participou. Exemplo:

```
+-------------------------------+
| Jogador | Vitórias | Partidas |
|---------|----------|----------|
| José    | 30       | 30       | 
| Carlos  | 20       | 25       | 
| Camila  | 15       | 35       | 
+-------------------------------+
```

##Funcionalidades

- Adicionar jogador: Permitir informar nome, quantidadeVitorias e quantidadePartidas;
- Adicionar vitória: incrementar quantidadeVitorias e quantidadePartidas do jogador;
- Adicionar partida: incrementar quantidadeVitorias e quantidadePartidas do jogador;
- Visualizar Ranking (Listar jogadores ordenado pelo número de vitórias, com as opçẽs de incluir vitória e incluir partida)

##Requisitos

API REST - Criar uma api em Java 8 com os serviços referentes às funcionalidades solicitadas utilizando BDD e TDD. Utilizar: Spring Boot, Cucumber e Mockito.

Front-End - Criar em uma tela todas as funcionalidades solicitadas. Utilizar: Qualquer versão do Angular.

Observações:

- Não é necessário criar autenticação para os serviços da API e Front-End.
- Fica a seu critério o banco de dados a ser utilizado.

##Avaliação 

Será avaliado os seguintes pontos:

- Qualidade e legibilidade do código;
- Tratamento de exceções;
- Qualidade dos testes;
- Tempo de entrega do desafio.
